use nickel::{Nickel, Request, Response};

pub fn bind_urls(server: &mut Nickel) {
    server.get("/", home);
}

fn home(request: &Request, response: &mut Response) {
    response.set_content_type("html");
    response.send("it works");
    response.send("<pre>");
    response.send(format!("request: {:?}", request));
    response.send("</pre>");
}
