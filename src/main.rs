extern crate http;
extern crate nickel;
extern crate debug;

use std::io::net::ip::Ipv4Addr;
use nickel::Nickel;

mod api;

fn main() {
    let listen_addr = Ipv4Addr(127, 0, 0, 1);
    let listen_port = 6767;
    let mut server = Nickel::new();

    api::bind_urls(&mut server);

    println!("Listening on {}:{}", listen_addr, listen_port);
    server.listen(listen_addr, listen_port);
}
